xcopy "%~dp0app\*.ini" "%appdata%\Power Gage\" /hryz
xcopy "%~dp0app\files" "%appdata%\Power Gage\files" /sihryz
xcopy "%~dp0extscr\images" "%appdata%\Power Gage\images" /sihryz
xcopy "%~dp0database\*.fdb" "%appdata%\Power Gage\database\" /sihryz

cd %~dp0
del /f /s /q dblink.ini 
del /f /s /q "%~dp0app\files"
del /f /s /q "%~dp0extscr\images"
del /f /s /q "%~dp0database\*.fdb"

xcopy "%~dp0extscr\*.ini" "%appdata%\Power Gage\" /hryz
xcopy "%~dp0tools\*.ini" "%appdata%\Power Gage\" /hryz

del /f /q "%~dp0extscr\*.ini"
del /f /q "%~dp0tools\*.ini"
del /f /q "%~dp0app\*.ini"

cd %~dp0
rd /s /q "%~dp0app\files"
rd /s /q "%~dp0extscr\images"

attrib +R /S "%appdata%\Power Gage\files\xls\*.xls"
attrib +R /S "%appdata%\Power Gage\files\xls\*.xlsx"