with tbl as (select le.*, loc.id_lifter
  from lifter_on_competition loc inner join
    lifter_exersis le on loc.id_lifter_compet = le.id_lifter_compet
  where
    loc.id_compet = %IDC% and loc.id_stream = %IDS%),
tbl2 as (
select 1 npp, 'squat' exer, tbl.squat1 weight, oc1 oc, id_lifter, id_lifter_compet as idLoc from tbl
union all
select 2, 'squat', tbl.squat2, oc2, id_lifter, id_lifter_compet from tbl
union all
select 3, 'squat', tbl.squat3, oc3, id_lifter, id_lifter_compet from tbl
union all
select 4, 'squat', tbl.squat4, oc10, id_lifter, id_lifter_compet from tbl
union all
select 5, 'bench', tbl.bench1, oc4, id_lifter, id_lifter_compet from tbl
union all
select 6, 'bench', tbl.bench2, oc5, id_lifter, id_lifter_compet from tbl
union all
select 7, 'bench', tbl.bench3, oc6, id_lifter, id_lifter_compet from tbl
union all
select 8, 'bench', tbl.bench4, oc11, id_lifter, id_lifter_compet from tbl
union all
select 9, 'dl', tbl.dl1, oc7, id_lifter, id_lifter_compet from tbl
union all
select 10, 'dl', tbl.dl2, oc8, id_lifter, id_lifter_compet from tbl
union all
select 11, 'dl', tbl.dl3, oc9, id_lifter, id_lifter_compet from tbl
union all
select 12, 'dl', tbl.dl4, oc12, id_lifter, id_lifter_compet from tbl)
, tbl3 as (
select t.id_lifter, min(t.idLoc) idLoc
  , coalesce((select max(weight) from tbl2 where id_lifter = t.id_lifter and oc is null), -1) as nextW
  , coalesce((select max(weight) from tbl2 where id_lifter = t.id_lifter and coalesce(oc, -1) = 1), -1) maxW
  , list(t.weight||case coalesce(oc, -1) when 0 then '(-)' when 1 then '(+)' else '(o)' end, '; ') descr
from tbl2 t
where weight is not null
group by id_lifter
)
select tbl4.*--l.name fio, wc.name wc, ac.name ageName
  , case nextW when -1 then 'none' else nextW end "����(��)"--nextW
  , case nextW when -1 then 'none' else cast(nextW*w.coef as numeric(8,4)) end as "����(����)" --ncoef

  , case maxW when -1 then 'none' else maxW end "�.(��)" --curW
  , case maxW when -1 then 'none' else cast(maxW*w.coef as numeric(8,4)) end as "�.(����.)"--ccoef
from  tbl3 inner join
  lifter_on_competition loc on tbl3.idLoc = loc.id_lifter_compet inner join
  (select * from light_table(%IDC%, %IDS%)) tbl4 on tbl4.loc = tbl3.idLoc inner join
--  lifter l on l.id_lifter = loc.id_lifter inner join
  weigth_category wc on wc.id_category = loc.id_category inner join
  age_class ac on ac.id_agecl = loc.id_agecl inner join
  wilks w on w.id_wilks = loc.id_wilks
order by tbl4.sex
  , wc.we
  , ac.from_age
  , tbl3.maxW desc

--select id_lifter, min(idLoc) idLoc, min(nextW) nextW, min(maxW) maxW from tbl3 group by tbl3.id_lifter


