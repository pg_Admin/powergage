# README #

Программа автоматизирующая проведение соревнований по Пауэрлифтингу и смежным дисциплинам.

* Стабильная версия 2.6

Программный комплекс для автоматизации процесса проведения соревнований по пауэрлифтингу включает в себя весь процесс, начиная с этапа приёма заявок заканчивая присвоением разрядов и распечаткой итоговых протоколов.

Так же позволяет секретарю вести соревнования в режиме он-лайн, обеспечивая участников следующей информацией:

*    Очередность выхода на помост
*    Табло борьбы (электронное табло)
*    Таймер обратного отсчёта совмещенный с информацией о спортсмене и весе на штанге
*    Монитор ассистентов отображающий набор веса на штангу. 

Автоматизации подверглись следующие федерации
** IPF, WPC, WPA, GPC, IPL, НАП, СПР, WRPF, Народный жим, Русский жим, Армлифтинг**
Адаптация программы возможна практически под любую федерацию пауэрлифтинга